# Code Retreat in a Box

## Agenda
* Introductions
* Describe structure of the day (6 45 minute sessions)
* Principle of Good Design – 4 rules of simple design – possibly use that to drive different challenges
* Session 1 – Pairing
* Session 2 – TDD like you mean it
* Session 3 – Methods cannot be longer than 4 lines long
* Session 4 – Verbs instead of Nouns
* Session 5 – No conditional logic
* Session 6 – Code Swap

### Closing Circle 
* What, if anything, did you learn today? 
* What, if anything, surprised you today? 
* What, if anything, will you do differently on Monday?

### Final Feedback
* What could we have done differently to make today better.

## Challenges
* TDD as if you meant it – stop and refactor you code after every test
* Missing tool – 
	* take away the mouse
	* don’t allow them to use any naked primitives – must be wrapped inside a higher level abstraction – avoids primitive obsession
* don’t allow them to use conditional statements (polymorphism)
* don’t use loops function 
* quality constraints – 4 lines of code per method or only 40 characters per line
* Verbs instead of Nouns – think about behavior instead of state
* Code swap – After session 5, ask people to standup, but don’t delete code. 

[Java Code Retreat Lightning Talk](https://www.youtube.com/watch?v=PKzznps5Z5g) by Jim Hurne

## What is a code retreat?
* 45 minute sessions
* Everyone pairs
* At the end of each session – delete code, do retrospective, switch pairs
* Make paper available for all pairs so they can do design together
* 6 sessions

## As a facilitator
* Your job is to make sure the attendees are getting value out of the session
* You do this by watching the code
* Session 1 – be a silent observer.  
* Ask questions to pairs – how did you decide upon that name for the variable or method?
* Make sure every has a clear understanding about the problem – display the rules

Do retrospective. Ask them to write down the things they liked about their code and one thing they really did not like about their it. Now ask pairs to change workstations and continue working on someone else’s code, trying to solve the thing the other pair didn’t like

## Retrospectives
* Take notes as you observe, so you remember what to talk about during retrospective
* When people ask questions, ask the group, even if you know the answer
* “That’s a great question – does anyone want to take a stab at answering it?”

## Deleting the Code
* Ask them to delete your code and stand up
* At work we’re always under a deadline, and never have the luxury to rewrite a chunk
* Don’t display the time left – and be vague when people ask how much time is left

Work with [mailto:sarah.c.gregory@intel.com](mailto:sarah.c.gregory@intel.com "Gregory, Sarah C") to setup the room and register session in MyLearning. Also let [Plavcan, Matthew W](mailto:matthew.w.plavcan@intel.com) know so he can post it on agile site

## Advertising
Contact [mailto:kristin.i.marshall@intel.com](mailto:kristin.i.marshall@intel.com "Marshall, Kristin I") to get blurb added to Chandler Communications/Bathroom Reader. Use some similar to this:

### *Arizona Code Retreat*

*Developers – Are you interested in improving your technical skills? Learning a new language? Learning how to pair program, refactor code and writing tests? Then come join your fellow nerds for a fun day of coding! Register at MyLearning (01288761)*

*Code retreat is a day-long, intensive practice event, focusing on the fundamentals of software development and design. It provides you with the opportunity to practice writing code, away from the pressures of 'getting things done'. The code retreat format has proven to be a highly effective means of skill improvement. Practicing the basic principles of modular and object-oriented design, developers can improve their ability to write code that minimizes the cost of change over time.*

*The entire day focuses on Conway’s Game of Life, a zero-player cellular automation game invented in 1970. The class will attempt to implement this simple game using a number of techniques and constraints. The goal of the class is not to complete the implementation, but to explore different ways to approach the design and take some of these insights back to your daily work.*

Send emails out to any coding forum you're aware of: 

* discussions-groups-agile@soco.intel.com
* discussions-groups-net--community--of--practice@soco.intel.com
* discussions-groups-software--professionals@soco.intel.com
* discussions-groups-it--ad@soco.intel.com
 
### A Couple Days Before Session
* Plan to get doughnuts for breakfast (Bosa's Doughnuts are great) Plan for about 1.5 times the number of students signed up.
* Chipotle for lunch – for orders of 20 or more, call the store a couple days in advance and make sure it’s OK. Tell them you will fax the order in by 9am for pickup at 11:30am [http://www.chipotle.com/FaxMenus/BurritosByTheBox/1718.pdf](http://www.chipotle.com/FaxMenus/BurritosByTheBox/1718.pdf)
* Line up helper to fax in the form and pickup lunch on Retreat day
* Buy Name Badges - not for names, but for a way to indicate amount of TDD knowledge (1-5 stars) and language preference/expertise
* Buy hand sanitizer and Zagg foam Gadget Cleansing Foam to wipe down keyboards/monitors

### Day of Code Retreat
* Print sign up sheet
* Print 2 copies of Chipotle Burrito in a Box sheet (one for them to place tick marks on and one to write up the final order to fax in)
* Print Game of Life Handouts
* Bring in doughnuts

#### Agenda
* 08:30 Opening
* 09:00 Iteration 1: Basic problem, no constraints (Facilitator: pair experienced TDD'er with novice)
* 10:00 Iteration 2: Short methods
* Bathroom break
* 11:00 Iteration 3: No conditionals [OR FOR EXPERTS: no return values]
* Lunch break - noon - 1:30
* 13:30 - Iteration 4: No primitives passed to methods / mid-iteration requirements change
* Bathroom break
* 14:30 Iteration 5: Mute Ping Pong
* 15:30 Iteration 6: Functional programming (no dynamic state)
* 16:30 Closing Circle - During discussions, focus on getting the attendees to ask each other questions, and reply to each other, not to you (emphasis: this is not a class)

#### Net Promoter
On a post-it, have them write a number (0-10), answering "How likely are you to recommend this activity to a friend or coworker?" and one sentence telling us how to improve the session.

Score is an average:

* 9 or 10: +1
* 7 or 8: 0
* 6 or less: -1


### Training and Background 
* Video: [Corey Haines Introduces Code Retreat](http://vimeo.com/18955165)
* Video: [Google Tech Talks - How to Write Clean, Testable Code - Miško Hevery](http://www.youtube.com/watch?v=XcT4yYu_TTs) Discusses the “My code is different” argument at 41:00
* Video: [Software Design in the 21st Century – Martin Fowler](https://www.youtube.com/watch?v=8kotnF6hfd8)   Economics of software at 45:00)\
* Article: [Your Design is Broken: It's 'TDD', not 'TDYAR'](http://geepawhill.org/?p=47) – Gee Paw Hill
* Article: [TDD is dead. Long live testing](http://david.heinemeierhansson.com/2014/tdd-is-dead-long-live-testing.html) – David Heinemeier Hansson started firestorm online; has some hangups with his TDD, so discards it wholesale and blogs about it.
* Book: [The Mikado Method](http://www.amazon.com/The-Mikado-Method-Ola-Ellnestam/dp/1617291218) – Ola Ellnestam - Legacy code strategy
* Book: [Coding Dojo Handbook](http://www.amazon.com/Coding-Dojo-Handbook-Emily-Bache/dp/919811803X) - Emily Bache
* Book: [Understanding the Four Rules of Simple Design](http://leanpub.com/4rulesofsimpledesign) – Corey Haines
* Code: [http://github.com/emilybache](http://github.com/emilybache) Refactoring Kata (Tennis, Gilded Rose, Yatzy)

 