# Code Dojo 

* Plan a regular cadence ahead of time, post it, get it on people’s calendars!
* Use well-known problems from the worldwide Agile community. Such as: 

•	String Calculator (Intro to TDD)
•	Video Store (Intro to Refactoring)
•	Tennis (Intro to SW Craftsmanship)
•	Bowling Game (Intro to SW Craftsmanship)
•	Gilded Rose (Improving Legacy Code)
•	Game of Life (Code Retreat)

