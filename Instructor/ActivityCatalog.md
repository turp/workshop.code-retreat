# Activity Catalog

## Format

**Name:** (if it is a concrete exercise)

**Description:** (how is it introduced and performed)

**Learning Goal:** (what are some of the ideas/experiences that people can focus on during the exercise)

**Preferred Session:** (do you like to do this in a particular session? Why? Why not?)

**Notes:** (any other comments about it)

## Basic Activities

Simple activities which are good for less-experienced groups.

### Ping pong

### Navigator-driver
### Missing Tool Activities

**Description:** Activities which involve removing a tool developers are used to using in order to help them learn how to use other tools more effectively.

* No Mouse
* Text editor only
* Paper only

## Missing Feature Activities

Activities which involve removing a common language feature developers are used to using in order to help them learn how to use higher-levels of abstraction to write better code.

### No naked primitives
### No conditional statements
### No loops

## Quality Constraint Activities

Activities which impose specific quality constraints to help developers practice a particular aspect of well-written code.

### Only four lines per method
### Immutables only, please

## Stretch Activities

Activities designed to stretch a group. Most of these activities tend to try to push developers into new ways of thinking about their code.

### Verbs instead of Nouns
### Code Swap
### Mute with find the loophole
### TDD as if you meant it


## Mute with Find the Loophole

> by Corey Haines

**Description:** This is a pairing exercise. It is actually three pairing techniques brought together in one exercise.

* Ping-pong - one person writes the tests, the other person writes the implementation code
* Mute - nobody can talk
* Find the Loophole - (AKA evil coder) the implementation person purposely writes the wrong algorithm that still makes the tests turn green. The key is that they have to keep the code very clean all the while. So, no big long if statements parsing on the input parameters.

**Learning Goal:** This is a great exercise on making expressive tests and taking that expressiveness to the extreme. If you can't talk to your pair, then you can't run on verbal assumptions. Communicating entirely through the tests highlights the idea of "tests as documentation" and can be considered a conversation with future you when you come back to maintain it.

**Preferred Session:** I like to do this at the session after lunch. I think it is a nice quieting down exercise after lunch. Plus, it really stretches people by putting them in a fairly uncomfortable position. By the time they come out of this, they are usually full immersed again in the sessions.

**Notes:** One thing I find funny is that, as soon as the timer goes off, and I say done, the room explodes in sound as people can't contain themselves anymore.I always let people have a few minutes (3-4) to talk with their pair before we gather together for the retro.
 
## Use only paper for the first 10 minutes of one iteration

**Description:** As simple as giving paper and pens to people and do not allow computers on for the first 10 minutes of the iteration.

**Learning Goal:** Scribbling in paper enhances communication, especially between pairs in which one person is dominant, because they cannot fly for the keyboard and start typing away leaving the other person behind. 

**Preferred Session:** The second session cause the first one has no rules; some people scribble just naturally but forcing people to do it could be a good exercise.

**Notes:** I tried it once before and during the retrospective a person brought up the fact that he thought that the process in paper was about design, and that it should not be done that way cause the design should be guided by tests. In contrast, the exercise is intended to be used as a communication aid and not as a design session in a waterfall sense. Most people thought it was very useful to clear their heads and make sure you are on the same page in the pair. Some of them stuck to it in following iterations using paper and the computer at the same time.


## No mouse
> by Kevin Rutherford

**Description:** No-one is allowed to touch the mouse during the session.

**Learning goal:** Programmers who know keyboard shortcuts work faster and are usually less distracted.

**Preferred session:** Worth doing early in the day to kick people out of their comfort zones. Session 2, maybe.

**Notes:** Not worth doing with a room full of vim ninjas!

## Changing requirements
by Rafael Devill

**Description:** Sometimes people request to have a longer double session where they have a chance to finish. In return for the longer session they need to respond to changes along the way. 

**Learning Goal:** A nice way to show participants how things they learned during the day can help back on the job. The changes are tailored in a way that they can not really be expected, and come at the worst possible time. It also shows why working on one story at a time is important.

**Preferred Session:** The last one. People learned a lot, and it's time take the new knowledge for a test drive. 

**Notes:** I wrote a blog on this [here](http://c0de-x.com/changing-requirements)

A possible set of requirement changes I used: 

* Infinite world
* Configurable rules 
* Store the age of each cell
* Cells have a weight they inherit from their ancestors in the creation step
* Trade show in 3 minutes: get as many tests as possible to a green status
 

## Legacy Pairing (not sure if the name fits very well)

**Description:** "Solve the problem as quickly and dirty as possible. Do not write clean code, do not create a good design, just forget anything you’ve ever learned about inner code quality. Simply get this thing running!" After 15 minutes, tell the pairs that one of their halfs will be gone and replace them with someone from another pair as the new guy who is now responsible for clean code, testing, etc.

**Learning Goal:** it's nearly impossible to transform the dirty code of the first 15 minutes into something more clean within the next 25 minutes. Socially, "old" guys in the pairs tend to keep their dirty code. "Broken window" effect can be observed: an existing dirty code base continues to grow.

**Preferred Session:** afternoon, maybe right after lunch as an energetic activity

**Notes:** I've written a short blog post on this [activity](http://agilecoach.de/2013-12-14/creating-legacy-behavior-in-15-minutes)

## Wormhole

Billed as the toughest [constraint](http://blog.adrianbolboaca.ro/2014/11/coderetreat-toughest-constraint)